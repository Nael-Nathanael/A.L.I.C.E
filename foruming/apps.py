from django.apps import AppConfig


class ForumingConfig(AppConfig):
    name = 'foruming'
