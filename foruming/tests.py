from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django_dynamic_fixture import G
import unittest
import time
from .views import index
from .forms import StatusForm
from .models import Status
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.firefox.options import Options


class LandingUnitTest(TestCase):

    def test_landing_is_exist_using_templates(self):
        response = Client().get('/index')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'landing.html')

    def test_content_hello(self):
        request = HttpRequest()
        response = index(request)
        htmlResponse = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', htmlResponse)

    def test_model_available(self):
        G(Status)
        G(Status)

        modelInDb = Status.objects.all()
        self.assertEquals(modelInDb.count(), 2)

    def test_form_available(self):
        form = StatusForm()
        self.assertIn("statText", form.as_p())

    def test_form_empty_input(self):
        data = {'statTest': ''}
        form = StatusForm(data)
        self.assertFalse(form.is_valid())

    def test_form_to_model(self):
        request = HttpRequest()
        request.method = "POST"
        request.POST['statText'] = "ViewTest"
        response = index(request)
        self.assertEqual(response.status_code, 200)


class CobaCobaTest(TestCase):
    def setUp(self):
        capabilities = {
            'browserName': 'chrome',
            'chromeOptions':  {
                'useAutomationExtension': False,
                'forceDevToolsScreenshot': True,
                'args': ['--headless', '--no-sandbox', '--disable-dev-shm-usage']
            }
        }
        self.browser = webdriver.Chrome(
            './chromedriver', desired_capabilities=capabilities)

    def tearDown(self):
        self.browser.quit()

    def test_insert_coba_coba(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/index')
        time.sleep(5)
        inputForm = browser.find_element_by_id("id_statText")
        inputForm.send_keys("Coba Coba")
        inputForm.submit()
        time.sleep(5)
        self.assertIn('Coba Coba', browser.page_source)
        self.tearDown()

    def test_check_title_exist(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/index')
        time.sleep(5)
        self.assertEqual("\'s Landing Page", browser.title)
        self.tearDown()

    def test_check_welcome_text(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/index')
        time.sleep(5)
        welcomeText = browser.find_element_by_id("welcomeText").text
        self.assertIn('Hello, Apa kabar?', welcomeText)
        self.tearDown()

    def test_welcome_text_has_desired_class(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/index')
        time.sleep(5)
        welcomeText = browser.find_element_by_id("welcomeText")
        self.assertIn("white", welcomeText.get_attribute("class"))
        self.assertIn("titleText", welcomeText.get_attribute("class"))
        self.tearDown()

    def test_welcome_bg_has_desired_class(self):
        browser = self.browser
        browser.get('http://alicization-begin.herokuapp.com/index')
        time.sleep(5)
        welcomeBg = browser.find_element_by_id("blurBlackBg")
        self.assertIn("centerBlack", welcomeBg.get_attribute("class"))
        self.tearDown()
