from django.apps import AppConfig


class GuessbookConfig(AppConfig):
    name = 'guessBook'
