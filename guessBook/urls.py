from django.urls import path
from .views import guest

urlpatterns = [
    path('guest-book', guest, name='guest'),
]