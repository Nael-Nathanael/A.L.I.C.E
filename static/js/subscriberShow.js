$(document).ready(function() {

    jQuery(function($) {
        $('.table').footable();
    });

    $.ajax({
        type: "POST",
        data: {
            asking: true,
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        dataType: "json",
        url: '/subscribe/getFullSub',
        success: function(result) {
            var txt = '';
            console.log("here success");
            var personCounter = 1;
            console.log(result.data.length);
            for (var i = 0; i < result.data.length; i++) {
                txt += '<tr>';
                txt += '<td class="numerus">' + personCounter + '</td>';
                txt += '<td>' + result.data[i].fields.username + '</td>';
                txt += '<td>' + result.data[i].fields.email + '</td>';
                txt += '<td>' + '<center><button class="unwise" onclick="removing(\'' + result.data[i].fields.email + '\')">Unsubscribe</button></center>' + '</td>';
                txt += '</tr>';
                personCounter++;
            }
            document.getElementById("ajaxer").innerHTML = txt;

        }
    });
});

function removing(email) {
    console.log("removing email : " + email);
    $.ajax({
        type: "POST",
        data: {
            toRemove: email,
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        dataType: "json",
        url: '/subscribe/remove',
        success: function(result) {
            var txt = '';
            console.log("here success");
            var personCounter = 1;
            console.log(result.data.length);
            for (var i = 0; i < result.data.length; i++) {
                txt += '<tr>';
                txt += '<td class="numerus">' + personCounter + '</td>';
                txt += '<td>' + result.data[i].fields.username + '</td>';
                txt += '<td>' + result.data[i].fields.email + '</td>';
                txt += '<td>' + '<center><button class="unwise" onclick="removing(\'' + result.data[i].fields.email + '\')">Unsubscribe</button></center>' + '</td>';
                txt += '</tr>';
                personCounter++;
            }
            document.getElementById("ajaxer").innerHTML = txt;
        }
    });
}