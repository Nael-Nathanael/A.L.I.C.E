$(function() {
  $("#st-accordion").accordion({
    oneOpenedItem: true
  });
});

var lightActive = false;

function toggleTheme() {
  if (lightActive) {
    darkFunction();
  } else {
    lightFunction();
  }
}

function lightFunction() {
  document.getElementById("line1").classList.add("darkText");
  document.getElementById("line1").classList.remove("lightText");
  document.getElementById("line2").classList.add("darkText");
  document.getElementById("line2").classList.remove("lightText");
  document.getElementById("line3").classList.add("darkText");
  document.getElementById("line3").classList.remove("lightText");
  document.getElementById("faq1").classList.add("darkContent");
  document.getElementById("faq1").classList.remove("lightContent");
  document.getElementById("faq2").classList.add("darkContent");
  document.getElementById("faq2").classList.remove("lightContent");
  document.getElementById("faq3").classList.add("darkContent");
  document.getElementById("faq3").classList.remove("lightContent");
  document.getElementById("faq4").classList.add("darkContent");
  document.getElementById("faq4").classList.remove("lightContent");
  document.getElementById("faq5").classList.add("darkContent");
  document.getElementById("faq5").classList.remove("lightContent");
  document.getElementById("faq6").classList.add("darkContent");
  document.getElementById("faq6").classList.remove("lightContent");
  document.getElementById("head").classList.add("darkOpaq");
  document.getElementById("head").classList.remove("lightOpaq");
  document.getElementById("myVideo").style.visibility = "hidden";
  lightActive = true;
}

function darkFunction() {
  document.getElementById("line1").classList.add("lightText");
  document.getElementById("line1").classList.remove("darkText");
  document.getElementById("line2").classList.add("lightText");
  document.getElementById("line2").classList.remove("darkText");
  document.getElementById("line3").classList.add("lightText");
  document.getElementById("line3").classList.remove("darkText");
  document.getElementById("faq1").classList.add("lightContent");
  document.getElementById("faq1").classList.remove("darkContent");
  document.getElementById("faq2").classList.add("lightContent");
  document.getElementById("faq2").classList.remove("darkContent");
  document.getElementById("faq3").classList.add("lightContent");
  document.getElementById("faq3").classList.remove("darkContent");
  document.getElementById("faq4").classList.add("lightContent");
  document.getElementById("faq4").classList.remove("darkContent");
  document.getElementById("faq5").classList.add("lightContent");
  document.getElementById("faq5").classList.remove("darkContent");
  document.getElementById("faq6").classList.add("lightContent");
  document.getElementById("faq6").classList.remove("darkContent");
  document.getElementById("head").classList.add("lightOpaq");
  document.getElementById("head").classList.remove("darkOpaq");
  document.getElementById("myVideo").style.visibility = "visible";
  document.getElementById("myVideo").play();
  lightActive = false;
}

$(document).ready(function() {
  $("#preloader").fadeOut(2000);
});
