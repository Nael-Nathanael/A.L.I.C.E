var total = 0;

function favMe(item) {
    $.ajax({
        method: "POST",
        url: "/shelf/newFav",
        headers: {
            "X-CSRFToken": $("[name=csrfmiddlewaretoken]").val(),
        },
        data: {
            bookId: item
        },
        success: function(result) {
            var bookIdString = '"' + result.bookId + '"';
            var newIcon = "<div id='" + result.bookId + "'class='favo changeFav' onclick='noFav(" + bookIdString + ")'></div>";
            console.log(newIcon);
            $("#" + result.bookId).replaceWith(newIcon);

            document.getElementById("total").innerText = "TOTAL = " + result.totalFav;
        }
    });
}

function noFav(itemId) {
    $.ajax({
        method: "POST",
        url: "/shelf/noFav",
        headers: {
            "X-CSRFToken": $("[name=csrfmiddlewaretoken]").val(),
        },
        data: {
            bookId: itemId
        },
        success: function(result) {
            var bookIdString = '"' + result.bookId + '"';
            var newIcon = "<div id='" + result.bookId + "'class='favo' onclick='favMe(" + bookIdString + ")'></div>";
            $("#" + result.bookId).replaceWith(newIcon);
            document.getElementById("total").innerText = "TOTAL = " + result.totalFav;
        }
    });
}