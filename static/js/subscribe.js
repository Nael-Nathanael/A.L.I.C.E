var email = false;
var username = false;
var password = false;
const normalUname = "Please insert 8 - 20 character username";
const normalEmail = "Enter valid Email, Please...";
const normalPass = "8 - 20 character you won't share with others";
const validUname = "Look's good to me";
const validEmail = "Nice One!!";
const validPass = "Nice Password !! ( I can't see it tho )";
const errUname = "Err ... 8 - 20 character ?";
const errEmail = "Invalid bruh";
const errEmail2 = "Already Registered, Please use another email";
const errPass = "Wait wait ... that's less than 8 character sir";
var emailInput = document.getElementById("id_email");
var usernameInput = document.getElementById("id_username");
var passwordInput = document.getElementById("id_password");
var theButton = document.getElementById("submit");
var theForm = document.getElementById("subForm");
var validEmailNotation = false;
var validEmailUnused = false;

$(document).ready(function() {
    $("#submit").prop('disabled', false);
    $("#submit").text('');
    usernameKeyCheck();
    usernameOutCheck();
    passwordKeyCheck();
    passwordOutCheck();
    checkMail(emailInput.value);
});

emailInput.onkeyup = function() {
    checkMail(this.value);
    checkValid();
};

usernameInput.onkeyup = function() {
    usernameKeyCheck();
};

usernameInput.onblur = function() {
    usernameOutCheck();
};

passwordInput.onkeyup = function() {
    passwordKeyCheck();
};

passwordInput.onblur = function() {
    passwordOutCheck();
};

theButton.onclick = function() {
    submitAjax();
}

function usernameKeyCheck() {
    $("#id_username").removeClass("check");
    $("#id_username").removeClass("wrong");
    $("#cauUsername").removeClass("red");
    $("#cauUsername").removeClass("green");
    $("#cauUsername").text(normalUname);
    username = false;
    if (usernameInput.value.length >= 8) {
        $("#id_username").addClass("check");
        $("#cauUsername").addClass("green");
        $("#cauUsername").text(validUname);
        username = true;
    }
    checkValid();
}

function usernameOutCheck() {
    if (usernameInput.value == "") {
        $("#id_username").removeClass("check");
        $("#id_username").removeClass("wrong");
        $("#cauUsername").removeClass("red");
        $("#cauUsername").removeClass("green");
    } else if (usernameInput.value.length < 8) {
        $("#id_username").removeClass("check");
        $("#id_username").addClass("wrong");
        $("#cauUsername").addClass("red");
        $("#cauUsername").text(errUname);
        username = false;
    }
}

function passwordKeyCheck() {
    $("#id_password").removeClass("check");
    $("#id_password").removeClass("wrong");
    $("#cauPassword").removeClass("red");
    $("#cauPassword").removeClass("green");
    $("#cauPassword").text(normalPass);
    password = false;
    if (passwordInput.value.length >= 8) {
        $("#id_password").addClass("check");
        $("#cauPassword").addClass("green");
        $("#cauPassword").text(validPass);
        password = true;
    }
    checkValid();
}

function passwordOutCheck() {
    if (passwordInput.value == "") {
        $("#id_password").removeClass("check");
        $("#id_password").removeClass("wrong");
        $("#cauPassword").removeClass("red");
        $("#cauPassword").removeClass("green");
    } else if (passwordInput.value.length < 8) {
        $("#id_password").removeClass("check");
        $("#id_password").addClass("wrong");
        $("#cauPassword").addClass("red");
        $("#cauPassword").text(errPass);
        password = false;
    }
}

function checkMail(value) {
    $("#id_email").removeClass("check");
    $("#id_email").removeClass("wrong");
    $("#cauEmail").removeClass("red");
    $("#cauEmail").removeClass("green");
    $("#cauEmail").text(normalEmail);
    email = false;
    if (value != "") {
        if (!emailInput.checkValidity()) {
            $("#id_email").removeClass("check");
            $("#id_email").removeClass("green");
            $("#id_email").addClass("wrong");
            $("#cauEmail").addClass("red");
            $("#cauEmail").text(errEmail);
            email = false;
        } else {
            var isiEmail = emailInput.value.split("@");
            if (isiEmail[1].includes(".")) {
                ajaxMail(value);
            } else {
                $("#id_email").removeClass("check");
                $("#cauEmail").removeClass("green");
                $("#id_email").addClass("wrong");
                $("#cauEmail").addClass("red");
                $("#cauEmail").text(errEmail);
            }
        }
        checkValid();
    }
}

function checkValid() {
    console.log("testing button");
    var form = $("#subForm");
    if (username && email && password) {
        console.log("button on with" + username + ' ' + email + ' ' + password);
        $("#submit").addClass('login100-form-btn-enable');
        $("#submit").prop('disabled', false);
        $("#submit").text('Subscribe!');

    } else {
        $("#submit").removeClass('login100-form-btn-enable');
        $("#submit").text('');
        $("#submit").prop('disabled', true);
    }
}

function invalidMail() {
    $("#id_email").removeClass("check");
    $("#id_email").addClass("wrong");
    $("#cauEmail").addClass("red");
    $("#cauEmail").text(errEmail2);
    email = false;
}

function ajaxMail(value) {
    $.ajax({
        type: "POST",
        data: {
            email: value,
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        dataType: "json",
        url: '/subscribe/check',
        beforeSend: function() {
            $('#id_email').removeClass("check");
            $('#id_email').removeClass("wrong");
            $('#cauEmail').removeClass("red");
            $('#cauEmail').removeClass("green");
            emailInput.setCustomValidity('');
            email = false;
        },
        success: function(result) {
            if (!result['is_taken']) {
                $('#id_email').addClass("check");
                $('#cauEmail').addClass("green");
                $('#cauEmail').text(validEmail);
                email = true;
            } else {
                invalidMail();
            }
        }
    });
    checkValid();
}

function submitAjax() {
    $.ajax({
        type: "POST",
        data: {
            email: emailInput.value,
            username: usernameInput.value,
            password: passwordInput.value,
            csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        dataType: "json",
        url: '/subscribe/createAccount',
        success: function(result) {
            if (result['objectCreated']) {
                alert("You have Subscribe, Yeay");
                theForm.reset();
                clearIcon();
            } else {
                alert("Error (?)")
            }
        }
    });
}

function clearIcon() {
    $("#id_email").removeClass("check");
    $("#id_email").removeClass("wrong");
    $("#id_username").removeClass("check");
    $("#id_username").removeClass("wrong");
    $("#id_password").removeClass("check");
    $("#id_password").removeClass("wrong");
    $('#cauEmail').text(normalEmail);
    $('#cauUsername').text(normalUname);
    $('#cauPassword').text(normalPass);
}