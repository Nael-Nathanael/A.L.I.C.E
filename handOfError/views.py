from django.shortcuts import render
from django.template import RequestContext
from django.http import HttpResponseServerError


def handler404(request, exception, template_name='error.html'):
    data = {
        'responseCode' : '404'
    }
    response = render(request, 'error.html', data)
    response.status_code = 404
    return response


def handler500(request):
    data = {
        'responseCode' : '500'
    }
    response = render(request, 'error.html', data)
    response.status_code = 500
    return response
