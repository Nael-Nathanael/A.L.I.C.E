from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime, date

myName = "Nathanael"
titlename = myName.upper
desc = "Hello World! I’m Nathanael, Web designer and programmer with 2 weeks of experience. Holding the record of Worldwide Handsome, thus having spirit to keep learning and never quit. I’ll try hard to create histories with my presence!"
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 9, 22)
address = "Griya 15, Depok, West Java"
email = "Evo220999@gmail.com"
phone = "+62-895-0338-6642"
nationality = "Indonesia"

def profile(request):
    response = {'myname': myName, 'age': calculate_age(birth_date.year),
                'desc': desc, 'address': address, 'email': email,
                'phone': phone, 'nationality': nationality, 'titlename': titlename}
    return render(request, 'profile.html', response)
    
def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0