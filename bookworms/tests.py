from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
import time
from .views import shelf, book_json
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class BookShelfTest(TestCase):
    def test_bookshelf_is_exist_using_templates(self):
        response = Client().get('/shelf')
        #not logged in yet
        self.assertEqual(response.status_code, 302)

# class SeleniumTest(TestCase):
#     def setUp(self):
#         capabilities = {
#             'browserName': 'chrome',
#             'chromeOptions':  {
#                 'useAutomationExtension': False,
#                 'forceDevToolsScreenshot': True,
#                 'args': ['--headless', '--no-sandbox', '--disable-dev-shm-usage']
#             }
#         }
#         self.browser = webdriver.Chrome(
#             './chromedriver', desired_capabilities=capabilities)

#     def tearDown(self):
#         self.browser.quit()

# table won't load if not logged in
    # def test_table_after_ajax(self):
    #     browser = self.browser
    #     browser.get('http://alicization-begin.herokuapp.com/shelf')
    #     time.sleep(5)
    #     getComic = browser.find_element_by_id("json2")
    #     getComic.click()
    #     time.sleep(5)
    #     table = browser.find_element_by_id("hideFirst")
    #     self.assertIn('Title', browser.page_source)
    #     self.tearDown()

# title would be different after redirecting
    # def test_check_title_exist(self):
    #     browser = self.browser
    #     browser.get('http://alicization-begin.herokuapp.com/shelf')
    #     time.sleep(5)
    #     self.assertEqual("\'s Shelf", browser.title)
    #     self.tearDown()

# can't favorite something if not logged in
    # def test_favorite(self):
    #     browser = self.browser
    #     browser.get('http://alicization-begin.herokuapp.com/shelf')
    #     time.sleep(5)
    #     getComic = browser.find_element_by_id("json2")
    #     getComic.click()
    #     time.sleep(5)
    #     total = browser.find_element_by_id("total")
    #     self.assertIn('TOTAL = 0', total.text)
    #     favo = browser.find_element_by_class_name("favo")
    #     favo.click()
    #     total = browser.find_element_by_id("total")
    #     self.assertIn('TOTAL = 1', total.text)