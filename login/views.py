from django.core import serializers
from django.shortcuts import render, HttpResponseRedirect, HttpResponse
from django.http import JsonResponse
from .models import Subscriber
from .forms import SubscriberForm
import json

def login(request):
    template = 'login.html'
    theForm = SubscriberForm()
    return render(request, 'login.html', {'form': theForm})

def checkEmail(request):
    data = {
        'is_taken': Subscriber.objects.filter(email=request.POST['email']).exists()
    }
    return JsonResponse(data)

def create(request):
    if (request.method == "POST"):
        theForm = SubscriberForm(request.POST)
        newUser = Subscriber(username=request.POST['username'], email=request.POST['email'], password=request.POST['password'])
        newUser.save()
        data = {
            'objectCreated' : True
        }
        return JsonResponse(data)

def viewAll(request):
    if (request.method == "POST"):
        if (request.POST['asking']):
            fullSubscriber = Subscriber.objects.all()
            jsonFullStr = serializers.serialize("json", fullSubscriber)
            jsonFull = json.loads(jsonFullStr)
            return JsonResponse({'data' : jsonFull})
        
def viewers(request):
    return render(request, 'thanks.html')

def removal(request):
    if (request.method == "POST"):
        Subscriber.objects.filter(email=request.POST['toRemove']).delete()
        fullSubscriber = Subscriber.objects.all()
        jsonFullStr = serializers.serialize("json", fullSubscriber)
        jsonFull = json.loads(jsonFullStr)
        return JsonResponse({'data' : jsonFull})