from django import forms
from .models import Subscriber


class SubscriberForm(forms.Form):
    username = forms.CharField(max_length=20, min_length=8, required=True, widget=forms.TextInput(
        attrs={'class': 'input100', 'placeholder': 'Username'}))
    email = forms.EmailField(required=True, widget=forms.EmailInput(
        attrs={'class': 'input100', 'placeholder': 'Email'}))
    password = forms.CharField(required=True, max_length=20, min_length=8, widget=forms.PasswordInput(
        attrs={'class': 'input100', 'placeholder': 'Password'}))
