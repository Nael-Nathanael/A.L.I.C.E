from django.db import models

class Subscriber(models.Model):
    username = models.CharField(max_length = 20)
    email = models.EmailField()
    password = models.CharField(max_length = 20)