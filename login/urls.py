from django.urls import path
from .views import login, checkEmail, create, viewAll, viewers, removal

urlpatterns = [
    path('subscribe', login, name='login'),
    path('subscribe/createAccount', create),
    path('subscribe/check', checkEmail),
    path('subscribe/getFullSub', viewAll),
    path('subscribe/viewers', viewers, name="viewers"),
    path('subscribe/remove', removal),
]