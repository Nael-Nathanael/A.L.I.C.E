from django.urls import path
from .views import theDoor, thyProfile, verify, imOut

urlpatterns = [
    path('let-Me-In-Please', theDoor, name='theDoor'),
    path('thyProfile', thyProfile, name='thyProfile'),
    path('verifyMe', verify, name='verify'),
    path('no-longer-here', imOut, name='no-longer-here'),
]