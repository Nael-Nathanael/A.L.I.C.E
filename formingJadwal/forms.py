from django import forms
from .models import JadwalPribadi
from django.forms.utils import ErrorList

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class JadwalPribadiForm(forms.ModelForm):
    class Meta:
        model = JadwalPribadi
        fields = ['tanggal','jam','nama_kegiatan','tempat','kategori']
        widgets = {
            'tanggal' : DateInput(),
            'jam' : TimeInput()
        }

class DivErrorList(ErrorList):
    def __str__(self):
        return 'This Field is Required ->   '